# twitch-proof-panel

twitch extension panel, stable before more iterations


### config view flow
On the broadcaster's extension manager,
the configuration view is presented during
installation.

```mermaid
graph TD
    A[Start] --> B(Connect Pavlok btn)
    B --> C{EBS valid}
    C -->|Yes| D(Save in Twitch cfg svc)
    H --> E(Input EBS base)
    E --> B
    
    D --> G(Open Pop-up)
    C -->|No| H(Warn EBS base)
    G --> I(Load content from EBS*)
    I --> J(fa:fa-lock Pavlok OAuth Login)
    J --> K{Authorize}
    K -->|Yes| L[Close Pop-up]
    K -->|No| M[Retry]
```

### Panel to EBS
EBS requires an authorization header with the token in order
to verify the  panel is using the correct 
Extension key. Another benefit of this token is
the broadcaster channel is included.

```mermaid
sequenceDiagram
    autonumber
    Panel->>+Twitch: onAuthorized
    Twitch-->>-Panel: token

    Panel->>+EBS: Authorization: Bearer token
    Note left of Panel: Button click event
    EBS-->>-Panel: Pavlok URL
    Panel->>+PavlokAPI: Pavlok URL
    PavlokAPI-->>-Panel: OAuth Login
```



